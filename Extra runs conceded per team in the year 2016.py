import matplotlib.pyplot as plt

import csv
global shortcuts, runsperteam
shortcuts=[]
runsperteam=[]
def calculate():
    global shortcuts, runsperteam
    with open('matches.csv', newline='') as csvfile:

        matchreader = csv.DictReader(csvfile)
        matchids=[]
        for details in matchreader:
            if details['season']=='2016':
                matchids.append(details['id'])
    print(matchids) 
    with open('deliveries.csv', newline='') as csvfile:

        matchreader = csv.DictReader(csvfile)
        teams=[]
        matches=[]
        for details in matchreader:
            if details['match_id'] in matchids:
                teams.append(details['bowling_team'])
        teams=sorted(list(set(teams)))
        csvfile.seek(0)
        runsperteam=[]
        for team in teams:
            csvfile.seek(0)
            extraruns=0
            for details in matchreader:
                if details['bowling_team'] == team and details['match_id'] in matchids:
                    extraruns+=int(details['extra_runs'])
                
            runsperteam.append(extraruns)
        
        print(runsperteam)
        combined=dict(zip(teams,runsperteam))
        print(combined)

    shortcuts=[i[0:3] for i in teams]

print(shortcuts)
def plot():
    global shortcuts, runsperteam
    fig, ax = plt.subplots()
    blabels = shortcuts
    ax.bar(blabels, runsperteam, label=blabels, color='green')

    ax.set_ylabel('Runsperteam')
    ax.set_title('Teams')
    ax.legend(title='Teams')

    plt.show()
    
calculate()
plot()   