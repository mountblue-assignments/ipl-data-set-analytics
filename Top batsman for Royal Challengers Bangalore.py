import csv
import matplotlib.pyplot as plt
global topplayers , runsscored
topplayers=[]
runsscored=[]

def calculate():
    with open('deliveries.csv', newline='') as csvfile:
        global topplayers , runsscored
        matchreader = csv.DictReader(csvfile)
        batters=[]
        for row in matchreader:
            
            if row['batting_team']=='Royal Challengers Bangalore':
                batters.append(row['batsman'])
        batters=list(set(batters))
        scores=[0]*len(batters)
        for i in range (len(batters)):
            total=0
            csvfile.seek(0)
        
            for row in matchreader:
                    
                    if row['batsman']==batters[i] and row['batting_team']=='Royal Challengers Bangalore':
                        total+=int(row['batsman_runs'])
            scores[i]=total
        playerandruns=dict(zip(scores,batters))
        playerandruns1 = reversed(sorted(playerandruns))
        sorted_playerandruns = {key:playerandruns[key] for key in playerandruns1}
        print(sorted_playerandruns)
        topplayers=list(sorted_playerandruns.values())
        runsscored=list(sorted_playerandruns.keys())
        print(topplayers[0:10],runsscored[0:10])
    
def plot() :
    global topplayers , runsscored      
    fig, ax = plt.subplots()

    blabels = topplayers[0:10]

    ax.bar(blabels, runsscored[0:10] , label=blabels, color='red')

    ax.set_ylabel('Total runs')
    ax.set_title('max runs scored by each team')
    # ax.legend(title='Teams')

    plt.show()

calculate()
plot()