import matplotlib.pyplot as plt
import csv

global matches ,seasons
seasons=[]
matches=[]
def calculate():
    with open('matches.csv', newline='') as csvfile:
        global matches,seasons
        matchreader = csv.DictReader(csvfile)
        
        for details in matchreader:
            
            seasons.append(details['season'])
        
        seasons=sorted(list(set(seasons)))
        print(seasons)
        
        for season in seasons:
            csvfile.seek(0)
            matchcount=0
            for details in matchreader:
                if details['season']==season:
                    matchcount+=1
            matches.append(matchcount)
        
        print(matches)

def plot():   
    global matches,seasons
    fig, ax = plt.subplots()


    blabels = seasons

    ax.bar(blabels, matches, label=blabels, color='green')

    ax.set_ylabel('Total matches')
    ax.set_title('total matches')
    # ax.legend(title='Teams')

    plt.show()
    

calculate()      
plot()     
      