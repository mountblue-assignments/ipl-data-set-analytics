import matplotlib.pyplot as plt
import csv
global teams,seasons
teams=[]
seasons=[]
def calculate():
    with open('matches.csv', newline='') as csvfile:
        global teams,seasons
        matchreader = csv.DictReader(csvfile)
        
        for details in matchreader:
            
            teams.append(details['team1'])
            seasons.append(details['season'])
            
        seasons=list(set(seasons))
        teams=list(set(teams))
        teams.sort()
        seasons.sort()
        # print(seasons,teams)
        countofteam=-1
        countofseason=-1
        matcheswonperteam=[]
        for team in teams:
            countofteam+=1
            matcheswonperteam.append([])
            for season in seasons:
                
                csvfile.seek(0)
                countofseason+=1
                matchwoncount=0
                for details in matchreader:
                    if details['season']==season and (details['team1']==team or details['team2']==team) and details['winner']==team:
                        matchwoncount+=1
                matcheswonperteam[countofteam].append(matchwoncount)
        teamcount=-1
        for i in matcheswonperteam:
            teamcount+=1
            print(i,teams[teamcount])
        for i in range(len(teams)):
            teams[i]=matcheswonperteam[i]     
        print(teams)
        
        
def plot():
    global teams,seasons
    fig, ax = plt.subplots()
    
    
    team2start=[teams[0][i]+teams[1][i] for i in range(len(teams[0]))]
    team3start=[team2start[i]+teams[2][i] for i in range(len(teams[0]))]
    team4start=[team3start[i]+teams[3][i] for i in range(len(teams[0]))]
    team5start=[team4start[i]+teams[4][i] for i in range(len(teams[0]))]
    team6start=[team5start[i]+teams[5][i] for i in range(len(teams[0]))]
    team7start=[team6start[i]+teams[6][i] for i in range(len(teams[0]))]
    team8start=[team7start[i]+teams[7][i] for i in range(len(teams[0]))]
    team9start=[team8start[i]+teams[8][i] for i in range(len(teams[0]))]
    team10start=[team9start[i]+teams[9][i] for i in range(len(teams[0]))]
    team11start=[team10start[i]+teams[10][i] for i in range(len(teams[0]))]
    team12start=[team11start[i]+teams[11][i] for i in range(len(teams[0]))]
    team13start=[team12start[i]+teams[12][i] for i in range(len(teams[0]))]
   
    ax.bar(seasons,teams[0],width=0.2,color='yellow',label='csk')
    ax.bar(seasons,teams[1],bottom=teams[0],width=0.2,color='black',label='dc')
    ax.bar(seasons,teams[2],bottom=team2start,width=0.2,color='pink',label='dd')
    ax.bar(seasons,teams[3],bottom=team3start,width=0.2,color='gold',label='gl')
    ax.bar(seasons,teams[4],bottom=team4start,width=0.2,color='tomato',label='kxip')
    ax.bar(seasons,teams[5],bottom=team5start,width=0.2,color='purple',label='ktk')
    ax.bar(seasons,teams[6],bottom=team6start,width=0.2,color='darkcyan',label='kkr')
    ax.bar(seasons,teams[7],bottom=team7start,width=0.2,color='cyan',label='mi')
    ax.bar(seasons,teams[8],bottom=team8start,width=0.2,color='red',label='pwi')
    ax.bar(seasons,teams[9],bottom=team9start,width=0.2,color='brown',label='rr')
    ax.bar(seasons,teams[10],bottom=team10start,width=0.2,color='bisque',label='rpsgs')
    ax.bar(seasons,teams[11],bottom=team11start,width=0.2,color='slategrey',label='rpsg')
    ax.bar(seasons,teams[12],bottom=team12start,width=0.2,color='lime',label='rcb')
    ax.bar(seasons,teams[13],bottom=team13start,width=0.2,color='forestgreen',label='srh')
    ax.set_ylabel('Total wins')
    ax.set_title('wins per team in a year')
    plt.legend(title='Teams',loc='upper right')
    plt.show()

calculate()
plot()

                
        
