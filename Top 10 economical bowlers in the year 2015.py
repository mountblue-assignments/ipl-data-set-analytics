import matplotlib.pyplot as plt
import csv

global worstbowlers, totalruns

worstbowlers=[]
totalruns=[]
def calculate():
    with open('matches.csv', newline='') as csvfile:
        global worstbowlers, totalruns
        matchreader = csv.DictReader(csvfile)
        matchids=[]
        for details in matchreader:
            if details['season']=='2015':
                matchids.append(details['id'])
    print(matchids)

    with open('deliveries.csv', newline='') as csvfile:

        matchreader = csv.DictReader(csvfile)
        bowlerslist=[]
        for details in matchreader:
            if details['match_id'] in matchids:
                bowlerslist.append(details['bowler'])
        bowlerslist=sorted(list(set(bowlerslist)))
        runsgiven=[]
        for bowler in bowlerslist:
            csvfile.seek(0)
            runs=0
            for details in matchreader:
                if details['bowler']== bowler and details['match_id'] in matchids:
                    runs+=int(details['total_runs'])
            runsgiven.append(runs)
        combined=dict(zip(runsgiven,bowlerslist))
        combined1 = sorted(combined)
        sorted_dict = {key:combined[key] for key in combined1}
        print(sorted_dict)
        worstbowlers=(list(sorted_dict.values()))
        totalruns=(list(sorted_dict.keys()))
        print( worstbowlers.reverse() , totalruns.reverse())
    
def plot():   
    fig, ax = plt.subplots()

    blabels = worstbowlers[0:10]

    ax.bar(blabels,    totalruns[0:10] , label=blabels, color='red')

    ax.set_ylabel('Total runs')
    ax.set_title('runs given')
    # ax.legend(title='Teams')

    plt.show()

calculate()
plot()