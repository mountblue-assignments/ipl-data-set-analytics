import csv
import matplotlib.pyplot as plt
global teams , runs
runs=[]
teams=[]
def calculate():
    with open("deliveries.csv", 'r') as file:
        deliveries = csv.DictReader(file)
        global teams 
        global runs
        for details in deliveries:
                teams.append(details['batting_team'])
        teams=list(set(teams))
        
        print(teams)
        for i in range(len(teams)):
            print(teams[i])
            total=0
            file.seek(0)
            for details in deliveries:
                    if details['batting_team']==teams[i]:
                        
                        total+=int(details['batsman_runs'])
            runs.append(total)

        print(runs)
    
def plot ():
        global teams 
        global runs
        fig, ax = plt.subplots()


        blabels = [i[0]+i[1]+i[2] for i in teams]



        ax.bar(blabels, runs, label=blabels, color='red')

        ax.set_ylabel('Total runs')
        ax.set_title('max runs scored by each team')
        # ax.legend(title='Teams')

        plt.show()

calculate()
plot()