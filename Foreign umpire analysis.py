import csv
import matplotlib.pyplot as plt
global totalcountries,noofumpires
totalcountries=[]
noofumpires=[]

def calculate():
    with open('matches.csv', newline='') as csvfile:
        global totalcountries,noofumpires
        matchreader = csv.DictReader(csvfile)
        umpires1=[]
        for details in matchreader:
            
            umpires1.append(details['umpire1'])
            umpires1.append(details['umpire2'])
            
        umpires1=list(set(umpires1))
        umpires1.sort()
        del umpires1[0]
        Nationality=['ind','ind','ind','nz','ind','ind','ind','pak',
                    'pak','aus','aus','aus','wi','ind','nz','ind',
                    'aus','ind','aus','sl','ind','sa','sa','ind',
                    'ind','ind','ind','ind','sa','eng','aus','ind',
                    'ind','aus','zim','aus','eng','ind','ind','ind',
                    'ind','aus','ind','ind','ind','ind','ind','ind',
                    'ind','ind','sl','ind','ind','ind']

    combined=dict(zip(umpires1,Nationality))
    print(combined) 


    totalcountries=list(set(Nationality))
    totalcountries.remove('ind')
    print(totalcountries)
    noofumpires=[]
    for i in totalcountries:
        noofumpires.append(Nationality.count(i))
    print(noofumpires)

def plot():
    global totalcountries,noofumpires
    fig, ax = plt.subplots()

    blabels = totalcountries

    ax.bar(blabels,    noofumpires    , label=blabels, color='red')

    ax.set_ylabel('noofumpires')
    ax.set_title('no umpires')
    # ax.legend(title='Teams')

    plt.show()
calculate()    
plot()